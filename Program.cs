﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UML2
{
  class Program
  {
    static void Main(string[] args)
    {
      GameFacade game = new GameFacade();
      game.StartGame();

      Console.ReadKey();
    }
  }

 /*
  * В программе представлено 2 паттерна (порождающий и структурный), т.к. поведенческий уже зачтен
  */

  /*
   * Порождающий паттерн Singleton для класса настроек игры GameSettings,
   * который содержит параметры игры, такие как здоровье игрока, количество врагов 
   * и урон оружия. 
   * Класс GameSettings имеет закрытый конструктор, чтобы предотвратить создание экземпляров извне, 
   * а доступ к его свойствам осуществляется через свойство Instance.
   */
  public class GameSettings
  {
    private static readonly GameSettings _instance = new GameSettings();

    public int PlayerHealth { get; set; }
    public int EnemyCount { get; set; }
    public int WeaponDamage { get; set; }

    private GameSettings()
    {
      // устанавливаем значения по умолчанию
      PlayerHealth = 100;
      EnemyCount = 3;
      WeaponDamage = 20;
    }

    public static GameSettings Instance
    {
      get { return _instance; }
    }
  }

  // структурный паттерн - Фасад
  public class GameFacade
  {
    private Player _player;
    private List<Enemy> _enemies;
    private Weapon _weapon;

    public GameFacade()
    {
      _player = new Player();
      _enemies = new List<Enemy>();
      _weapon = new Weapon(GameSettings.Instance.WeaponDamage);

      // создаем врагов
      for (int i = 0; i < GameSettings.Instance.EnemyCount; i++)
      {
        Console.WriteLine("Создан враг!");
        _enemies.Add(new Enemy());
      }
    }

    public void StartGame()
    {
      // выводим настройки игры
      Console.WriteLine("-----------------------------");
      Console.WriteLine("SETTINGS");
      Console.WriteLine($"Player health: {GameSettings.Instance.PlayerHealth}");
      Console.WriteLine($"Number of enemies: {GameSettings.Instance.EnemyCount}");
      Console.WriteLine($"Weapon damage: {GameSettings.Instance.WeaponDamage}");
      Console.WriteLine("-----------------------------");

      // начинаем игру
      foreach (Enemy enemy in _enemies)
      {
        int player_damage = _weapon.Attack();
        int enemy_damage = enemy.Attack();
        while (enemy.Health > 0 && _player.Health > 0)
        {
          Console.WriteLine("Player attacks " + enemy.index+" enemy.");
          enemy.TakeDamage(player_damage);
          if (enemy.Health <= 0)
          {
            Console.WriteLine(enemy.index + " enemy has been defeated!");
            break;
          }
          Console.WriteLine(enemy.index + " enemy attacks Player");
          _player.TakeDamage(enemy_damage);
          Console.WriteLine("Player Health: " + _player.Health);
          if (_player.Health > 0)
          {
            Console.WriteLine(enemy.index + " enemy HP: " + enemy.Health);
          }
          else
          {
            Console.WriteLine("Player's HP < 0.");
        
            break;
          }
          
        }
        if (_player.Health <= 0)
        {
          Console.WriteLine("Gave over");
          break;
        }
      }
      if (_player.Health>0)
        Console.WriteLine("Congratulations! You have defeated all enemies!");

    }
  }

  public class Player
  {
    public int Health { get; set; }

    public Player()
    {
      Health = GameSettings.Instance.PlayerHealth;
    }

    public void TakeDamage(int damage)
    {
      Health -= damage;
    }
  }

  public class Enemy
  {
    public int Health { get; set; }

    public static int count = 1;
    public int index;

    public Enemy()
    {
      Health = 50;
      index = count;
      count++;
    }

    public void TakeDamage(int damage)
    {
      Health -= damage;
    }

    public int Attack()
    {
      return 10;
    }
  }

  public class Weapon
  {
    private int _damage;

    public Weapon(int damage)
    {
      _damage = damage;
    }

    public int Attack()
    {
      return _damage;
    }
  }



}
